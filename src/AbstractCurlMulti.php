<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use Generator;
use SplObjectStorage;

/**
 * AbstractCurlMulti serves as the base class for handling multi-cURL operations.
 * This abstract class provides shared functionality for managing multiple cURL requests simultaneously.
 * Concrete implementations should extend this class and implement required abstract methods for specific cURL operations.
 *
 * @version 3.0.2
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractCurlMulti implements CurlMultiInterface {
	
	/**
	 * @var SplObjectStorage
	 * @since   3.0.0 First time introduced.
	 */
	private SplObjectStorage $_requests;
	
	/**
	 * Time, in seconds, to wait for a response.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	private int $_connectionTimeout = self::DEFAULT_CONNECTION_TIMEOUT;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct() {
		//
		$this->_requests = new SplObjectStorage();
	}
	
	/**
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionTimeout() : int {
		return $this->_connectionTimeout;
	}
	
	/**
	 * @param    int    $seconds
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionTimeout(int $seconds) : static {
		//
		$this->_connectionTimeout = $seconds;
		
		//
		return $this;
	}
	
	/**
	 * @return null|Generator
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionRequestMulti() : ?Generator {
		// Loop the requests
		$a = $this->_requests;
		$a->rewind();
		
		// Loop through the requests and yield their CurlHandle objects
		while($a->valid()):
			// Get CurlHandle from the current request
			if(( $c = $a->current() ) && $c instanceof CurlInterface):
				yield $a->getInfo() => $c->getConnection();
			endif;
			
			// Goto next request
			$a->next();
		endwhile;
		
		// Return null when no more requests are available
		return NULL;
	}
	
	/**
	 * @param    CurlInterface    $connection
	 * @param    null|string      $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequestMulti(CurlInterface $connection, ?string $name = NULL) : static {
		// Save the connection and its name in the request storage
		$this->_requests->offsetSet($connection->setName($name), $name);
		
		//
		return $this;
	}
}