<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use Tiat\Connection\Curl\Exception\InvalidArgumentException;
use Tiat\Connection\Curl\Exception\RuntimeException;
use Tiat\Standard\DataModel\HttpMethod;

use function extension_loaded;
use function is_string;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractCurl extends AbstractCurlConnection implements CurlInterface {
	
	/**
	 * @var HttpMethod
	 * @since   3.0.0 First time introduced.
	 */
	private HttpMethod $_requestMethod = HttpMethod::GET;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_url;
	
	/**
	 * @var array|string
	 * @since   3.0.0 First time introduced.
	 */
	private array|string $_queryParams;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct() {
		if(! extension_loaded('curl')):
			throw new RuntimeException("PHP extension 'curl' is required for this class.");
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRequestMethod() : static {
		//
		$this->_requestMethod = HttpMethod::GET;
		
		//
		return $this;
	}
	
	/**
	 * Returns the URL of the current request.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequest() : ?string {
		return $this->getUrl();
	}
	
	/**
	 * Returns the URL of the current request.
	 *
	 * @return string|null The URL of the current request, or null if the URL is not available.
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string {
		return $this->_url ?? NULL;
	}
	
	/**
	 * Sets the URL for the request.
	 *
	 * @param    null|string    $url    The URL for the request.
	 *
	 * @return static The updated object.
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(?string $url = NULL) : static {
		//
		if(! empty($url)):
			$this->_url = $url;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Set the URL for the request.
	 * This method will replace setUrl() method at future.
	 *
	 * @param    NULL|string    $url
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addRequest(?string $url = NULL) : static {
		return $this->setUrl($url);
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : static {
		//
		unset($this->_url);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueryParams() : static {
		//
		unset($this->_queryParams);
		
		//
		return $this;
	}
	
	/**
	 * Returns the request method.
	 *
	 * @return string The request method.
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestMethod() : string {
		return $this->_requestMethod->value;
	}
	
	/**
	 * @param    HttpMethod|string    $method
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestMethod(HttpMethod|string $method) : static {
		//
		if(is_string($method)):
			// Value is a string, try to convert it to an HttpMethod object
			if(( $value = HttpMethod::tryFrom($method) ) !== NULL):
				$this->_requestMethod = $value;
			else:
				// Value is null, or it's not a valid HTTP method
				$msg = sprintf("Invalid HTTP method provided: %s", $method);
				throw new InvalidArgumentException($msg);
			endif;
		else:
			// Value if instance of HttpMethod
			$this->_requestMethod = $method;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryParams() : array|string|null {
		return $this->_queryParams ?? NULL;
	}
	
	/**
	 * @param    array|string    $query
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryParams(array|string $query) : static {
		//
		$this->_queryParams = $query;
		
		//
		return $this;
	}
	
}
