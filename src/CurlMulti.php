<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use CurlHandle;
use CurlMultiHandle;
use Laminas\Uri\UriFactory;
use Tiat\Connection\Curl\Exception\InvalidArgumentException;

use function curl_multi_add_handle;
use function curl_multi_close;
use function curl_multi_exec;
use function curl_multi_getcontent;
use function curl_multi_info_read;
use function curl_multi_init;
use function curl_multi_remove_handle;
use function curl_multi_select;
use function curl_multi_strerror;
use function filter_var;
use function get_debug_type;
use function is_int;
use function sprintf;

/**
 * The CurlMulti class is a wrapper for the curl_multi_* functions in PHP,
 * which allows for the simultaneous processing of multiple cURL requests.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class CurlMulti extends AbstractCurlMulti {
	
	/**
	 * @var CurlMultiHandle
	 * @since   3.0.0 First time introduced.
	 */
	private CurlMultiHandle $_multiHandle;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionInfo;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionResult;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionError;
	
	/**
	 * @param    CurlInterface|string    $request
	 * @param    NULL|string             $name
	 * @param    null|string             $order
	 *
	 * @return CurlMulti
	 * @since   3.0.0 First time introduced.
	 */
	public function setMultiRequest(CurlInterface|string $request, ?string $name = NULL, ?string $order = NULL) : static {
		// Set request to the multi handle as a CurlInterface object
		if($request instanceof CurlInterface):
			// Check that the connection has a valid name
			$this->setConnectionRequestMulti($request, $name);
		else:
			// Create an CurlInterface and check that URL is valid
			$this->setConnectionRequestMulti($this->_createRequest($request, $name), $name);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Creates and returns a new Curl request.
	 *
	 * @param    string         $uri     The URI for the request.
	 * @param    null|string    $name    The name associated with the request.
	 *
	 * @return CurlInterface The instantiated Curl object implementing CurlInterface.
	 * @since   3.0.0 First time introduced.
	 */
	private function _createRequest(string $uri, ?string $name = NULL) : CurlInterface {
		// Validate and normalize the URI
		if(( filter_var($uri, FILTER_VALIDATE_URL) === FALSE ) || ( UriFactory::factory($uri) )->isValid() === FALSE):
			$msg = sprintf('Invalid or invalid URL: %s', $uri);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return ( new Curl($name) )->setConnectionRequest($uri);
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : static {
		//
		if(( $handler = $this->getCurlMultiHandler() ) !== NULL):
			// Add requests to the multi handle
			$this->addRequestsToHandler();
			
			//
			$active = 0;
			
			// Run the requests until all are completed or an error occurs
			do {
				//
				curl_multi_select($handler, $this->getConnectionTimeout());
				$this->_runActiveConnection($handler, $active);
				
				//
				while(( $info = curl_multi_info_read($handler) ) !== FALSE):
					//
					$info = (array)$info;
					if(( $result = $info['result'] ?? 0 ) !== CURLE_OK):
						$this->setConnectionError($this->_getErrorMessage($result));
					endif;
				endwhile;
			} while($active);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|CurlMultiHandle
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurlMultiHandler() : ?CurlMultiHandle {
		// Create a new CurlMultiHandle
		if(empty($this->_multiHandle)):
			if(( $handler = curl_multi_init() ) instanceof CurlMultiHandle):
				$this->_multiHandle = $handler;
			else:
				throw new InvalidArgumentException('Failed to create CurlMultiHandle');
			endif;
		endif;
		
		//
		return $this->_multiHandle ?? NULL;
	}
	
	/**
	 * Add requests to the CurlMultiHandle.
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function addRequestsToHandler() : static {
		if(( $handler = $this->getCurlMultiHandler() ) !== NULL):
			// Loop through the requests and add them to the multi-handle
			foreach($this->getConnectionRequestMulti() as $connection):
				if($connection instanceof CurlHandle):
					curl_multi_add_handle($handler, $connection);
				else:
					$this->_curlHandleError($connection);
				endif;
			endforeach;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Throws an InvalidArgumentException if the given connection is not a valid CurlHandle.
	 *
	 * @param $connection
	 *
	 * @return never
	 * @since   3.0.0 First time introduced.
	 */
	private function _curlHandleError($connection) : never {
		$msg = sprintf('Invalid connection: %s. CurlHandle required, got type: %s', $connection,
		               get_debug_type($connection));
		throw new InvalidArgumentException($msg);
	}
	
	/**
	 * Executes the given CurlMultiHandle and manages active transfers.
	 *
	 * @param    CurlMultiHandle    $handler    The Curl multi-handle resource.
	 * @param    int                $active     Reference to the number of active connections.
	 *
	 * @return int Returns 0 upon completion.
	 * @since   3.0.0 First time introduced.
	 */
	protected function _runActiveConnection(CurlMultiHandle $handler, int &$active) : int {
		// Do we have any active transfers?
		do {
			if((int)( $status = curl_multi_exec($handler, $active) > 0 )):
				$this->setConnectionError(curl_multi_strerror((int)$status));
			endif;
		} while($status === ( CURLM_CALL_MULTI_PERFORM || $active ));
		
		//
		return 0;
	}
	
	/**
	 * Returns a descriptive error message based on the given cURL error code.
	 *
	 * @param    int    $error    The cURL error code.
	 *
	 * @return string A descriptive error message corresponding to the provided error code.
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getErrorMessage(int $error) : string {
		return match ( $error ) {
			CURLE_UNSUPPORTED_PROTOCOL => "libcurl used a protocol that this libcurl does not support",
			CURLE_URL_MALFORMAT => "The URL was not properly formatted",
			CURLE_COULDNT_RESOLVE_HOST => "Couldn't resolve host. The given remote host was not resolved.",
			CURLE_COULDNT_CONNECT => "Failed to connect to host or proxy",
			CURLE_HTTP_NOT_FOUND => "The HTTP server returns an error code that is >= 400",
			CURLE_OUT_OF_MEMORY => "A memory allocation request failed",
			default => sprintf('Error: CURLE(%u)', $error)
		};
	}
	
	/**
	 * @param    NULL|string    $name
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponse(?string $name = NULL) : ?array {
		// Get connection result if the multi-handle is not empty
		if(( $handler = $this->getCurlMultiHandler() ) !== NULL):
			// Get response
			foreach($this->getConnectionRequestMulti() as $key => $connection):
				if($connection instanceof CurlHandle):
					// Get response & save them as key => value
					$this->setConnectionResult($key, curl_multi_getcontent($connection));
					
					// Save connection info & error
					$this->setConnectionInfo($key, curl_getinfo($connection));
					$this->setConnectionError(curl_error($connection));
					
					// Remove connection from the multi-handle
					curl_multi_remove_handle($handler, $connection);
				else:
					$this->_curlHandleError($connection);
				endif;
			endforeach;
			
			// Close multi handler
			curl_multi_close($handler);
		endif;
		
		//
		return $this->getConnectionResult($name);
	}
	
	/**
	 * @param    NULL|string    $key
	 *
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult(?string $key = NULL) : array|string|null {
		if($key === NULL):
			return $this->_connectionResult ?? NULL;
		else:
			return $this->_connectionResult[$key] ?? NULL;
		endif;
	}
	
	/**
	 * @param    string         $key
	 * @param    null|string    $value
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(string $key, ?string $value = NULL) : static {
		//
		$this->_connectionResult[$key] = $value;
		
		//
		return $this;
	}
	
	/**
	 * @param    NULL|string|int    $key
	 *
	 * @return null|array|string
	 * @see     curl_getinfo()
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo(string|int|null $key = NULL) : array|string|null {
		if(is_int($key)):
			return $this->_connectionInfo[$key] ?? NULL;
		else:
			return $this->_connectionInfo ?? NULL;
		endif;
	}
	
	/**
	 * @param    string    $key
	 * @param    array     $info
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionInfo(string $key, array $info) : static {
		//
		$this->_connectionInfo[$key] = $info;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : ?array {
		return $this->_connectionError ?? NULL;
	}
	
	/**
	 * @param    string    $error
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionError(string $error) : static {
		//
		if(! empty(trim($error))):
			$this->_connectionError[] = $error;
		endif;
		
		//
		return $this;
	}
}