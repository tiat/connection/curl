<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use CurlHandle;
use Tiat\Standard\DataModel\HttpMethod;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CurlInterface extends CurlConnectionInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string RESPONSE_ALL = 'all';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string RESPONSE_HEADERS = 'headers';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string RESPONSE_BODY = 'body';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string RESPONSE_HTTP_CODE = 'http_code';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string RESPONSE_LAST_URL = 'last_url';
	
	/**
	 * Shutdown the connection if open.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void;
	
	/**
	 * @return null|CurlHandle
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : ?CurlHandle;
	
	/**
	 * Get the connection name.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * Set the connection name.
	 *
	 * @param    string    $name
	 *
	 * @return CurlInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : CurlInterface;
	
	/**
	 * Returns the request method.
	 *
	 * @return string The request method.
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestMethod() : string;
	
	/**
	 * Sets the request method.
	 *
	 * @param    HttpMethod    $method
	 *
	 * @return CurlInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestMethod(HttpMethod $method) : CurlInterface;
	
	/**
	 * Resets the request method to its default value.
	 *
	 * @return CurlInterface
	 */
	public function resetRequestMethod() : CurlInterface;
	
	/**
	 * Returns the URL of the current request.
	 *
	 * @return string|null The URL of the current request, or null if the URL is not available.
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * Sets the URL for the request.
	 *
	 * @param    string    $url    The URL for the request.
	 *
	 * @return CurlInterface The updated object.
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(string $url) : CurlInterface;
	
	/**
	 * Resets the URL to its default value.
	 *
	 * @return CurlInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : CurlInterface;
	
	/**
	 * Get the query parameters for the current request.
	 *
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getQueryParams() : array|string|null;
	
	/**
	 * As with curl_setopt(), passing an array to CURLOPT_POST will encode the data as multipart/form-data,
	 * while passing a URL-encoded string will encode the data as application/x-www-form-urlencoded.
	 *
	 * @param    array|string    $query
	 *
	 * @return CurlInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setQueryParams(array|string $query) : CurlInterface;
	
	/**
	 * Resets the query parameters & delete them all.
	 *
	 * @return CurlInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetQueryParams() : CurlInterface;
	
	/**
	 * Get the response information from the last curl request.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseInfo() : ?array;
	
	/**
	 * Get the error message from the response.
	 *
	 * @return string|null The error message from the response, or null if there is no error.
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseError() : ?string;
	
	/**
	 * Retrieves the response data based on the specified section.
	 *
	 * @param    string    $section    The section of the response to retrieve. Default is RESPONSE_BODY.
	 *
	 * @return array|string|null The response data. Returns null if the section does not exist.
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponse(string $section = self::RESPONSE_BODY) : array|string|null;
}
