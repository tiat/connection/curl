<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CurlConnectionInterface {
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionTimeout() : ?int;
	
	/**
	 * @param    int    $timeout
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionTimeout(int $timeout) : CurlConnectionInterface;
	
	/**
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionTimeout() : CurlConnectionInterface;
	
	/**
	 * Get the connection request URL.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionRequest() : ?string;
	
	/**
	 * Sets the connection request URL.
	 *
	 * @param    string    $url    The URL of the connection request.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequest(string $url) : CurlConnectionInterface;
	
	/**
	 * @param    string    $url
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionRequest(string $url) : CurlConnectionInterface;
	
	/**
	 * Set connection username.
	 *
	 * @param    string    $password
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPassword(string $password) : CurlConnectionInterface;
	
	/**
	 * Reset connection password.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionPassword() : CurlConnectionInterface;
	
	/**
	 * Set connection username.
	 *
	 * @param    string    $username
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionUsername(string $username) : CurlConnectionInterface;
	
	/**
	 * Reset connection username.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionUsername() : CurlConnectionInterface;
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionPort() : ?int;
	
	/**
	 * @param    int    $port
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPort(int $port) : CurlConnectionInterface;
	
	/**
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionPort() : CurlConnectionInterface;
	
	/**
	 * Validates the connection request by checking if the given URI is valid.
	 *
	 * @param    string    $uri    The URI to be validated.
	 *
	 * @return bool True if the connection request is valid, false otherwise.
	 * @since   3.0.0 First time introduced.
	 */
	public function validateConnectionRequest(string $uri) : bool;
	
	/**
	 * Set connection options.
	 *
	 * @param    array    $options
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOptions(array $options) : CurlConnectionInterface;
	
	/**
	 * @param    int      $option      Curl option constant.
	 * @param    mixed    $value       Value for the option.
	 * @param    bool     $override    Overwrite existing option value.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOption(int $option, mixed $value, bool $override) : CurlConnectionInterface;
	
	/**
	 * Get connection options.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptions() : ?array;
	
	/**
	 * Returns the headers of the HTTP request.
	 *
	 * @return array|null The headers of the HTTP request or null if no headers are set.
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaders() : ?array;
	
	/**
	 * @param    array    $headers
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Changed return type to CurlConnectionInterface.
	 */
	public function setHeaders(array $headers) : CurlConnectionInterface;
	
	/**
	 * Sets a header for the request.
	 *
	 * @param    string    $header    The header to be set.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Changed return type to CurlConnectionInterface.
	 */
	public function setHeader(string $header) : CurlConnectionInterface;
	
	/**
	 * Resets the headers to their default value.
	 *
	 * @return CurlConnectionInterface
	 * @since   3.0.0 First time introduced.
	 * @since   3.1.0 Changed return type to CurlConnectionInterface.
	 */
	public function resetHeaders() : CurlConnectionInterface;
	
	/**
	 * Check that is there a specific header in the request and use $header as a like operator. Case doesn't matter.
	 *
	 * @param    string    $header
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasHeader(string $header) : bool;
}
