<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use CurlMultiHandle;
use Generator;

/**
 * The CurlMultiInterface interface defines the methods that a class implementing it
 * must implement to be a valid CURL multi handle.
 * This interface extends the CurlConnectionInterface, which means it includes all the methods
 * from the CurlConnectionInterface as well.
 * A CURL multi handle allows multiple CURL handles to be processed simultaneously in parallel.
 * This can be useful for making multiple HTTP requests or performing other network operations
 * concurrently.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     CurlConnectionInterface
 * @link    https://www.php.net/manual/en/class.curl-multi-handle.php
 */
interface CurlMultiInterface {
	
	/**
	 * Default connection timeout for the multi-handle.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const int DEFAULT_CONNECTION_TIMEOUT = 30;
	
	/**
	 * Get connection timeout for the multi-handle.
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionTimeout() : int;
	
	/**
	 * Sets connection timeout for the multi-handle.
	 *
	 * @param    int    $seconds
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionTimeout(int $seconds) : CurlMultiInterface;
	
	/**
	 * @return null|Generator
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionRequestMulti() : ?Generator;
	
	/**
	 * Sets a connection request to a multi-handle.
	 *
	 * @param    CurlInterface    $connection    The CURL connection to be added.
	 * @param    string           $name          The name of the request.
	 *
	 * @return CurlMultiInterface The multi-handle with the added request.
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequestMulti(CurlInterface $connection, string $name) : CurlMultiInterface;
	
	/**
	 * @param    CurlInterface|string    $request
	 * @param    string                  $name
	 * @param    string                  $order
	 *
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMultiRequest(CurlInterface|string $request, string $name, string $order) : CurlMultiInterface;
	
	/**
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : CurlMultiInterface;
	
	/**
	 * @return null|CurlMultiHandle
	 * @since   3.0.0 First time introduced.
	 */
	public function getCurlMultiHandler() : ?CurlMultiHandle;
	
	/**
	 * Add requests to the CurlMultiHandle.
	 *
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function addRequestsToHandler() : CurlMultiInterface;
	
	/**
	 * @param    string    $name
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponse(string $name) : ?array;
	
	/**
	 * @param    string    $key
	 *
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult(string $key) : array|string|null;
	
	/**
	 * @param    string    $key
	 * @param    string    $value
	 *
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(string $key, string $value) : CurlMultiInterface;
	
	/**
	 * @param    string|int    $key
	 *
	 * @return null|array|string
	 * @see     curl_getinfo()
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo(string|int $key) : array|string|null;
	
	/**
	 * @param    string    $key
	 * @param    array     $info
	 *
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionInfo(string $key, array $info) : CurlMultiInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : ?array;
	
	/**
	 * @param    string    $error
	 *
	 * @return CurlMultiInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionError(string $error) : CurlMultiInterface;
}
