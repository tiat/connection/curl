<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use Laminas\Uri\Uri;
use Tiat\Standard\DataModel\HttpMethod;

use function array_key_exists;
use function array_replace_recursive;
use function array_unique;
use function defined;
use function filter_var;
use function implode;
use function is_array;
use function preg_match;
use function strtolower;

/**
 * Abstract class AbstractCurlConnection
 * This abstract class provides a base implementation for curl Connection.
 * Classes extending this class should implement the necessary methods
 * to establish a curl connection specific to their needs.
 *
 * @version 3.0.2
 * @since   3.0.0 First time introduced.
 * @since   3.1.0 Fixed method names for consistency and documentation. Also, response interfaces has been updated.
 */
abstract class AbstractCurlConnection implements CurlConnectionInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const int DEFAULT_CONNECTION_TIMEOUT = 5000;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_connectionTimeout = self::DEFAULT_CONNECTION_TIMEOUT;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionHeaders;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_connectionOptions;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_connectionRequest;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_connectionUsername;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_connectionPassword;
	
	/**
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	private int $_connectionPort;
	
	/**
	 * @param    NULL|string    $url
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionRequest(?string $url = NULL) : static {
		//
		unset($this->_connectionRequest);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $uri
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateConnectionRequest(string $uri) : bool {
		return (bool)filter_var($uri, FILTER_VALIDATE_URL);
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionTimeout() : static {
		//
		$this->_connectionTimeout = self::DEFAULT_CONNECTION_TIMEOUT;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionOptions() : ?array {
		return $this->_connectionOptions ?? NULL;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionOptions() : ?array {
		// Set HTTP request method
		$options[CURLOPT_CUSTOMREQUEST] = $this->getRequestMethod();
		
		// Set connection options
		$options =
			array_replace_recursive($this->_defaultConnectionOptions() ?? [], $this->getConnectionOptions() ?? []);
		
		// As default GET request params are in the query string
		if($this->getRequestMethod() === HttpMethod::GET->value):
			$request = new Uri($this->getConnectionRequest());
			$this->setConnectionRequest($request->setQuery($this->getQueryParams())->__toString());
		else:
			// Define the POST request & data if provided
			// Set POST data if provided
			$options[CURLOPT_POST] = 1;
			
			// POST related headers & options
			if(! empty($data = $this->getQueryParams())):
				// Set POST data as form-data
				$options[CURLOPT_POSTFIELDS] = $data;
				
				// Add the content
				if(is_array($data) && $this->hasHeader('content-type') === FALSE):
					$this->setHeader('Content-Type: multipart/form-data');
				endif;
			endif;
		endif;
		
		// Set connection headers if provided
		if(! empty($headers = $this->getHeaders())):
			$options[CURLOPT_HTTPHEADER] = array_values($headers);
		endif;
		
		// Define alternative port
		if(( $port = $this->getConnectionPort() ) !== NULL && $port > 0):
			$options[CURLOPT_PORT] = $port;
		endif;
		
		// Set connection username and password if provided
		if(! empty($username = $this->_getConnectionUsername())):
			$options[CURLOPT_USERPWD] = $username . ':' . ( $this->_getConnectionPassword() ?? '' );
		endif;
		
		//
		return $options;
	}
	
	/**
	 * @param    array    $options
	 * @param    bool     $override
	 *
	 * @return AbstractCurlConnection
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOptions(array $options, bool $override = FALSE) : static {
		//
		$this->_setConnectionOptions($options, override:$override);
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 * @since   3.0.1 Added support for HTTP/3.
	 */
	protected function _defaultConnectionOptions() : array {
		// Define HTTP version for CURL.
		// Add support for HTTP/3 (since PHP 8.4.0)
		if(1 === 2 && PHP_VERSION_ID >= 80400 && defined('CURL_HTTP_VERSION_3')):
			#$CURL_HTTP_VERSION = constant('CURL_HTTP_VERSION_3');
		else:
			$CURL_HTTP_VERSION = CURL_HTTP_VERSION_NONE;
		endif;
		
		//
		return [CURLOPT_HEADER => TRUE, CURLOPT_FAILONERROR => FALSE, CURLOPT_RETURNTRANSFER => TRUE,
		        CURLOPT_SSL_VERIFYPEER => TRUE, CURLOPT_SSL_VERIFYHOST => 2, CURLOPT_TIMEOUT => 30,
		        CURLOPT_HTTP_VERSION => $CURL_HTTP_VERSION, CURLOPT_SASL_IR => TRUE, CURLOPT_SSL_ENABLE_ALPN => TRUE,
		        CURLOPT_ENCODING => "",
		        CURLOPT_CONNECTTIMEOUT_MS => ( $this->getConnectionTimeout() ?? self::DEFAULT_CONNECTION_TIMEOUT )];
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionTimeout() : ?int {
		return $this->_connectionTimeout ?? NULL;
	}
	
	/**
	 * @param    int    $timeout
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionTimeout(int $timeout) : static {
		//
		$this->_connectionTimeout = $timeout;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionRequest() : ?string {
		//
		if(empty($this->_connectionRequest)):
			$this->setConnectionRequest();
		endif;
		
		//
		return $this->_connectionRequest ?? NULL;
	}
	
	/**
	 * @param    NULL|string    $url
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionRequest(?string $url = NULL) : static {
		if($this->validateConnectionRequest($value = ( $url ?? $this->getUrl() )) === TRUE):
			$this->_connectionRequest = $value;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $header
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function hasHeader(string $header) : bool {
		return (bool)preg_match('/' . strtolower($header) . '/', strtolower(implode(' ', $this->getHeaders())));
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaders() : ?array {
		//
		if(! empty($this->_connectionHeaders)):
			return array_unique($this->_connectionHeaders, SORT_REGULAR);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Sets a header for the request.
	 *
	 * @param    string    $header    The header to be set.
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeader(string $header) : static {
		//
		$this->_connectionHeaders[] = $header;
		
		// Make sure the headers are unique
		if(! empty($this->_connectionHeaders)):
			$this->_connectionHeaders = array_unique($this->_connectionHeaders);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionPort() : ?int {
		return $this->_connectionPort ?? NULL;
	}
	
	/**
	 * @param    int    $port
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPort(int $port) : static {
		//
		$this->_connectionPort = $port;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionUsername() : ?string {
		return $this->_connectionUsername ?? NULL;
	}
	
	/**
	 * @param    string    $username    The username to set.
	 *
	 * @return $this The current instance of the CurlInterface.
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionUsername(string $username) : static {
		//
		$this->_connectionUsername = $username;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getConnectionPassword() : ?string {
		return $this->_connectionPassword ?? NULL;
	}
	
	/**
	 * @param    array       $options
	 * @param    NULL|int    $key
	 * @param    bool        $override
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	private function _setConnectionOptions(array $options, ?int $key = NULL, bool $override = FALSE) : void {
		if(! empty($options)):
			foreach($options as $option => $value):
				if($option === CURLOPT_HTTPHEADER):
					$this->setConnectionOption($key ?? $option, $value, $override);
				else:
					if(is_array($value)):
						$this->_setConnectionOptions($value, $option, $override);
					else:
						$this->setConnectionOption($key ?? $option, $value, $override);
					endif;
				endif;
			endforeach;
		endif;
	}
	
	/**
	 * @param    int      $option
	 * @param    mixed    $value
	 * @param    bool     $override
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionOption(int $option, mixed $value, bool $override = FALSE) : static {
		if($override === TRUE || ! $this->_hasConnectionOption($option)):
			$this->_connectionOptions[$option] = $value;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    int    $option
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	private function _hasConnectionOption(int $option) : bool {
		return array_key_exists($option, $this->_connectionOptions ?? []);
	}
	
	/**
	 * @param    string    $password
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionPassword(string $password) : static {
		//
		$this->_connectionPassword = $password;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionPassword() : static {
		//
		unset($this->_connectionPassword);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionUsername() : static {
		//
		unset($this->_connectionUsername);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionPort() : static {
		//
		unset($this->_connectionPort);
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $headers
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeaders(array $headers) : static {
		//
		foreach($headers as $value):
			$this->setHeader($value);
		endforeach;
		
		//
		return $this;
	}
	
	/**
	 * Resets the headers to their default value.
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHeaders() : static {
		//
		unset($this->_connectionHeaders);
		
		//
		return $this;
	}
}
