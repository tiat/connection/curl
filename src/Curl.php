<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Curl
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Curl;

//
use CurlHandle;
use Tiat\Connection\Curl\Exception\InvalidArgumentException;
use Tiat\Connection\Curl\Exception\RuntimeException;

use function ctype_alnum;
use function curl_close;
use function curl_error;
use function curl_getinfo;
use function curl_init;
use function curl_setopt;
use function is_array;
use function register_shutdown_function;
use function sprintf;

/**
 * @version 3.0.2
 * @since   3.0.0 First time introduced.
 */
class Curl extends AbstractCurl {
	
	/**
	 * @var CurlHandle
	 */
	private CurlHandle $_connection;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_responseResult;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_responseInfo;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_responseError;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_response;
	
	/**
	 * @var string
	 */
	private readonly string $_name;
	
	/**
	 * @param    NULL|string    $name
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?string $name = NULL) {
		//
		register_shutdown_function([$this, 'shutdown']);
		
		//
		if(! empty($name)):
			$this->setName($name);
		endif;
		
		//
		parent::__construct();
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown() : void {
		if(( $curl = $this->getConnection() ) !== NULL):
			curl_close($curl);
		endif;
	}
	
	/**
	 * Gets the connection handle.
	 *
	 * @return CurlHandle|null The connection handle if it exists, otherwise null.
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnection() : ?CurlHandle {
		// Set connection request if none exists
		if(empty($this->_connection)):
			$this->_setConnection();
		endif;
		
		// Return connection if exists
		return $this->_connection ?? NULL;
	}
	
	/**
	 * @return Curl
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConnection() : static {
		// Set connection options if none exists
		if(empty($this->_connection)):
			// Create a new Curl session
			if(! ( ( $conn = curl_init() ) instanceof CurlHandle )):
				throw new RuntimeException("Curl session can't be initialized.");
			endif;
			
			// Set connection options if none exists
			if(! empty($options = $this->_getConnectionOptions())):
				// Loop through the options and set them
				foreach($options as $option => $value):
					if($option === CURLOPT_HTTPHEADER):
						#var_dump($option, $value);
						$this->_setConnectionOptionValue($conn, $option, (array)$value);
					else:
						if(is_array($value) && ! array_is_list($value)):
							foreach($value as $k => $v):
								$this->_setConnectionOptionValue($conn, $k, $v);
							endforeach;
						else:
							$this->_setConnectionOptionValue($conn, $option, $value);
						endif;
					endif;
				endforeach;
			endif;
			
			// Set the connection url at last because it might be required to modify the request url
			$this->_setConnectionOptionValue($conn, CURLOPT_URL, $this->getConnectionRequest());
			
			// Set the connection handle
			$this->_connection = $conn;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_name ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//Var is readonly type but don't throw an exception
		if(empty($this->_name)):
			if(ctype_alnum($name) === TRUE):
				$this->_name = $name;
			else:
				$msg = sprintf("Name can contain only alphanumeric character(s). Got '%s'", $name);
				throw new InvalidArgumentException($msg);
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $section
	 *
	 * @return null|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponse(string $section = self::RESPONSE_BODY) : array|string|null {
		//
		$this->_setResponse()->_parseResponse();
		
		// Return the response based on the section
		if($section === self::RESPONSE_ALL):
			return $this->_response ?? NULL;
		else:
			return $this->_response[$section] ?? NULL;
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResponse() : static {
		// Get Curl connection
		if(( $curl = $this->getConnection() ) !== NULL):
			// Save result
			if(( $response = curl_exec($curl) ) !== FALSE):
				$this->_setResponseResult($response);
			endif;
			
			// Get connection info for log
			$this->_setResponseInfo(curl_getinfo($curl));
			
			// Collect error(s) if any
			if(! empty($error = curl_error($curl))):
				$this->_setResponseErrors($error);
			endif;
		else:
			throw new RuntimeException('Failed to get Curl connection.');
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _parseResponse() : void {
		//
		if(( $conn = $this->getConnection() ) !== NULL):
			if(( $error = $this->getResponseError() ) === NULL):
				
				if(( $response = $this->_getResponseResult() ) !== NULL):
					// Header size is required at first
					$headerSize = curl_getinfo($conn, CURLINFO_HEADER_SIZE);
					
					// Read the header and body
					$result[self::RESPONSE_HEADERS]   = substr($response, 0, $headerSize);
					$result[self::RESPONSE_BODY]      = substr($response, $headerSize);
					$result[self::RESPONSE_HTTP_CODE] = curl_getinfo($conn, CURLINFO_HTTP_CODE);
					$result[self::RESPONSE_LAST_URL]  = curl_getinfo($conn, CURLINFO_EFFECTIVE_URL);
					
					// Move data to response
					$this->_response = $result;
				endif;
			else:
				// Error occurred
				throw new RuntimeException($error);
			endif;
		else:
			// No connection available
			throw new RuntimeException('Failed to get Curl connection.');
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseError() : ?string {
		return $this->_responseError ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getResponseResult() : ?string {
		return $this->_responseResult ?? NULL;
	}
	
	/**
	 * @param    string    $result
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResponseResult(string $result) : static {
		//
		$this->_responseResult = $result;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseInfo() : ?array {
		return $this->_responseInfo ?? NULL;
	}
	
	/**
	 * @param    array    $info
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResponseInfo(array $info) : static {
		//
		$this->_responseInfo = $info;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $error
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResponseErrors(string $error) : static {
		//
		$this->_responseError = $error;
		
		//
		return $this;
	}
	
	/**
	 * Sets the value of a CURL option for the given CURL handle.
	 *
	 * @param    CurlHandle    $curlHandle    The CURL handle.
	 * @param    int           $option        The CURL option to set.
	 * @param    mixed         $value         The value to set for the CURL option.
	 *
	 * @throws InvalidArgumentException if unable to set the CURL option.
	 * @since   3.0.0 First time introduced.
	 */
	private function _setConnectionOptionValue(CurlHandle $curlHandle, int $option, mixed $value) : void {
		if($option > 0 && ! curl_setopt($curlHandle, $option, $value)):
			$msg = sprintf("Can't set the curl option '%u' with value '%s'.", $option, $value);
			throw new InvalidArgumentException($msg);
		endif;
	}
}
